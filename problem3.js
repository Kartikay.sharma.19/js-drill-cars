const res = function(inventory){

    if(  inventory === undefined  || inventory.constructor !== Array){
        const arr = [];
        return arr ;
    }


    for(let i = 0; i< inventory.length ; i++){
        for(let j = i+1; j<inventory.length ; j++){
            if(inventory[i]. car_model.toUpperCase() > inventory[j].car_model.toUpperCase()){
                let x = inventory[i].car_model ;
                inventory[i].car_model = inventory[j].car_model
                inventory[j].car_model = x
            }
        }
    }
     return inventory
}
module.exports = res

