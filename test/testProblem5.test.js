const problem4 = require('../problem4');
const  inventory = require('./inventory');
const problem5 = require('../problem5');

const ListOfYear =  [...problem4(inventory)];
const listOfOlderCars = [...problem5(ListOfYear)];

test('testing problem5', () => {
    expect(problem5(ListOfYear)).toEqual([
        1983, 1990, 1995, 1987, 1996,
        1997, 1999, 1987, 1995, 1994,
        1985, 1997, 1992, 1993, 1964,
        1999, 1991, 1997, 1992, 1998,
        1965, 1996, 1995, 1996, 1999
      ]);
    expect(listOfOlderCars.length).toBe(25);
});