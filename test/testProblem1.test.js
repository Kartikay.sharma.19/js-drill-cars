const inventory = require('./inventory')

const problem1 = require('../problem1')

// console.log(problem1(inventory)) ;
test(' Testing problem1', () => {
     expect(problem1(inventory)).toBe('car 33 is a 2011 Jeep Wrangler')
});
