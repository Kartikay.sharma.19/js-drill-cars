 const { expect } = require('@jest/globals')
const problem2 = require('../problem2')
const inventory = require('./inventory')

// console.log(problem2(inventory));

test('Testing problem2', () => {
    expect(problem2(inventory)).toBe('Last car is a Lincoln Town Car')

})
