const result = (yearList) => {
    if(  yearList === undefined  || yearList.constructor !== Array){
        const arr = [];
        return arr ;
    }
    let olderCars = [];
   
    for(let i =0; i< yearList.length;i++){
        if(yearList[i] < 2000){
            olderCars.push(yearList[i]);
        }
    }
    return olderCars;
}

module.exports = result;