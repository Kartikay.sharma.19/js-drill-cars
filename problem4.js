const inventory = require("./test/inventory");

const result = inventory => {
    if(  inventory === undefined  || inventory.constructor !== Array){
        const arr = [];
        return arr ;
    }
    let car_years = [];
    for(let index = 0; index < inventory.length; index++) {
        car_years.push(inventory[index].car_year);
    }
    return car_years;
}
module.exports = result;